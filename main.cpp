#include <iostream>
#include <functional>
#include <chrono>

using namespace std::chrono;

int64_t f(int64_t x)
{  
    if(x == 0 || x == 1) 
        return 1; 
    else return f(x-1) + f(x-2);  
}

template<typename T>
int64_t f2_fab(int64_t x, T f2)
{
    return f2(x,f2);
}

int main()
{
    int iters = 100000;
    
    std::function<int64_t(int64_t)> f1 = [&f1](int64_t x)->int64_t {  if(x == 0 || x == 1) return 1; else return f1(x-1) + f1(x-2);  };
    
    auto f2 = [](int64_t n) 
    {
        auto lambda = [](int64_t x, const auto& ff)->int64_t {  if(x == 0 || x == 1) return 1; else return ff(x-1,ff) + ff(x-2,ff);  };

        return lambda(n, lambda);
    };

    std::cout << "Lambda in C++14 tests" << std::endl;
    
    auto start1 = steady_clock::now();
    for(int i=0;i<iters; i++)
        auto res = f(i%30);
    auto end1 = steady_clock::now();
    auto diff1  = end1 - start1;   


    auto start2 = steady_clock::now();
    for(int i=0;i<iters; i++)
        auto res2 = f1(i%30);
    auto end2 = steady_clock::now();
    auto diff2  = end2 - start2;   

    auto start3 = steady_clock::now();
    for(int i=0;i<iters; i++)
        auto res3 = f2(i%30);
    auto end3 = steady_clock::now();
    auto diff3  = end3 - start3;   

    

    std::cout << "duration (normal function)  : " << duration_cast<std::chrono::milliseconds> (diff1).count() << " ms" << std::endl;
    std::cout << "duration (std::function)    : " << duration_cast<std::chrono::milliseconds> (diff2).count() << " ms" << std::endl;
    std::cout << "duration (auto)             : " << duration_cast<std::chrono::milliseconds> (diff3).count() << " ms" << std::endl;

}
