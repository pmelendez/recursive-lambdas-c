#Recursive lambdas in C++(14)

This is a quick test to benchmark the two different approaches to perform recursive lambdas in C++14.


Since one of the approaches uses generic lambdas, we need to use a compiler that support that feature.
This was originally tested on Clang/LLVM 3.5.0 and gcc 4.9.2


These commands were run:

    clang++ -std=c++14 -stdlib=libc++ main.cpp -o lambda_test
    clang++ -std=c++14 -stdlib=libc++ main.cpp -o lambda_test_opt -O3
    g++-4.9 -std=c++1y main.cpp -o lambda_test_gcc 
    g++-4.9 -std=c++1y main.cpp -o lambda_test_gcc_opt -O3


And these were the corresponding outputs:



    $ ./lambda_test
    Lambda in C++14 tests
    The results are: r1 (normal function) =14930352, r2 (std::function) =14930352, r3 (auto)=14930352
    duration (normal function)  : 63 ms
    duration (std::function)    : 428 ms
    duration (auto)             : 80 ms

    $ ./lambda_test_opt
    Lambda in C++14 tests
    The results are: r1 (normal function) =14930352, r2 (std::function) =14930352, r3 (auto)=14930352
    duration (normal function)  : 27 ms
    duration (std::function)    : 68 ms
    duration (auto)             : 27 ms

    $ ./lambda_test_gcc
    Lambda in C++14 tests
    The results are: r1 (normal function) =14930352, r2 (std::function) =14930352, r3 (auto)=14930352
    duration (normal function)  : 61 ms
    duration (std::function)    : 533 ms
    duration (auto)             : 68 ms

    $ ./lambda_test_gcc_opt
    Lambda in C++14 tests
    The results are: r1 (normal function) =14930352, r2 (std::function) =14930352, r3 (auto)=14930352
    duration (normal function)  : 18 ms
    duration (std::function)    : 57 ms
    duration (auto)             : 17 ms

